public class arrayTest {

    // 数组的实是一块地址连续的内存,就像是编号连续的一沓白纸;
    // 数组的名, 就是这个地址连续内存的第一个内存的地址.
    // 数组的变量和基本变量一样,本身就是一个地址, 但是与基本变量不一样的是,这个地址的值,是数组的名,也就是数组的第一个地址.

    /*  数组的长度
    .length获取数组的长度;
    数组创建后,长度不可以改变
    数组索引过界的错误叫做:IndexOutOfBoundException
    数组里每个元素都有初始值,初始值和类型有关.对于数字类型,初始值是0; 对于boolean类型,初始值是false;
    */

    /*  让变量指向新的数组
    数组变量可以指向新的数组实体. 这时候, 数组变量的值就是新的数组实体的地址, 这种数组变量的赋值操作,叫做让变量指向新的数组.
    如果没有别的数组变量指向原来的数组实体,原来的数组实体就再也不可访问了.
    对于非基本类型的变量,计算机都要通过这种两级跳的方式来访问.
     */

    public static void main(String[] args) {
        int[] myIntArray = new int[88];
        System.out.println(myIntArray[4]);
        System.out.println(myIntArray[87]);

        double[] myDoubleArray = new double[77];
        System.out.println(myDoubleArray[6]);
        System.out.println(myDoubleArray[76]);

        String[] myStringArray = new String[77];
        System.out.println(myStringArray[6]);
        System.out.println(myStringArray[76]);

        System.out.println("myDoubleArray的长度是:"+myDoubleArray.length);
    }
}
