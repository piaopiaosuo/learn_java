import java.util.Scanner;

public class scoreMaster {
    public static void main(String[] args) {
        int YuWen = 0;
        int ShuXue = 1;
        int WaiYu = 2;
        int Wuli = 3;
        int Huaxue = 4;
        int Shengwu = 5;

        int totleScoreCount = 6;

        String[] scoreNames = new String[totleScoreCount];
        scoreNames[YuWen] = "语文";
        scoreNames[ShuXue] = "数学";
        scoreNames[WaiYu] = "外语";
        scoreNames[Wuli] = "物理";
        scoreNames[Huaxue] = "化学";
        scoreNames[Shengwu] = "生物";

        Scanner in = new Scanner(System.in);
        System.out.println("请问要保存几年的成绩?");
        int yesToSave = in.nextInt();
        double[][] scores = new double[yesToSave][scoreNames.length];
        for (int j = 0; j < yesToSave; j++) {
            for (int i = 0; i < totleScoreCount; i++) {
                scores[j][i] = 80 + Math.random() * 20;
                System.out.println("第" + (j + 1) + "年" + scoreNames[i] + "这门课的成绩是:" + scores[j][i]);
            }
        }
        boolean cont = true;
        while (cont) {
//            System.out.println("请问要查看第几年的成绩?");
//            int toSeeYear = in.nextInt();
//            if (toSeeYear == -1) {
//                System.out.println("这是要退出了");
//                cont = false;
//                break;
//            }
//            System.out.println("请问要查看哪一科的成绩?");
//            int toSeeIndex = in.nextInt();
//            System.out.println("你要查看的科目是:" + scoreNames[toSeeIndex - 1] + ",它的成绩是:" +
//                    scores[toSeeYear - 1][toSeeIndex - 1]);
            System.out.println("请输入要进行的操作编号:");
            System.out.println("1:求某年最好成绩; 2:求某年的平均成绩; 3: 求所有年份的最好成绩; 4: 求某门课历年最好成绩");
            int num = in.nextInt();
            switch (num) {
                case 1:
                    System.out.println("请输入要进行的年份:");
                    int year = in.nextInt() - 1;
                    if (year < 0 || year > yesToSave) {
                        System.out.println("非法的年份");
                        break;
                    }
                    double bestScore = 0;
                    int ind = 0;
                    for (int i = 0; i < scores[year].length; i++) {
                        if (scores[year][i] > bestScore) {
                            ind = i;
                            bestScore = scores[year][i];
                        }
                    }
                    System.out.println("第" + (year + 1) + "年的最好成绩科目是:" + scoreNames[ind] + ",成绩是:" + scores[year][ind]);
                    break;
                case 2:
                    System.out.println("请输入要进行的年份:");
                    int towYear = in.nextInt() - 1;
                    if (towYear < 0 || towYear > yesToSave) {
                        System.out.println("非法的年份");
                        break;
                    }
                    double sum = 0;
                    for (int i = 0; i < scores[towYear].length; i++) {
                        sum += scores[towYear][i];
                    }
                    System.out.println("第" + (towYear + 1) + "年的平均成绩是:" + sum);
                    System.out.println("第" + (towYear + 1) + "年的平均成绩是:" + sum / scores[towYear].length);
                    break;
                case 3:
                    int threeYear = 0;
                    int threIndex = 0;
                    double threeScore = 0;
                    for (int i = 0; i < scores.length; i++) {
                        for (int j = 0; j < scores[i].length; j++) {
                            if(scores[i][j]> threeScore){
                                threeScore = scores[i][j];
                                threeYear = i;
                                threIndex = j;
                            }
                        }
                    }
                    System.out.println("第"+(threeYear+1)+"年的"+scoreNames[threIndex]+"科目的成绩最高,为:"+threeScore);
                default:
                    System.out.println("程序退出");
                    cont = false;
            }
        }
    }
}
