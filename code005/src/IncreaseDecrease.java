public class IncreaseDecrease {
    public static void main(String[] args) {
        int a = 1;
        System.out.println("a++=" + a++);  // ++ 在后面先用,再++
//        System.out.println("a++=" + a);
//        a = a + 1;
//        a += 1;
        System.out.println("a=" + a);
        int b = 10;
        System.out.println("b--=" + b--);
        System.out.println("b=" + b);

        a = 1;
        System.out.println("a++=" + ++a);  // ++ 在前面,先++,再用
        System.out.println("a=" + a);
        b = 10;
        System.out.println("b--=" + --b);
        System.out.println("b=" + b);
    }
}
