public class forTest {
    // for(初始语句;循环体条件表达式; 循环体后语句){
    //      for 循环体
    // }
    public static void main(String[] args) {
//        for(int i=0; i < 10; i++){
//            System.out.println("i的值是:"+i);
//        }
        char ch = 'A';
        int num = ch;  // 自动类型转换,python中不会自动转换, 会报错
        for(int i = 0; i < 26; i++){
            System.out.println(num+"\t"+(char)(num++));
        }
    }
}
