public class MathClass {
    public static void main(String[] args){
//        System.out.println(5+6);
//        System.out.println(5*6);
//        System.out.println(5-2.0);
//        System.out.println("求余");
//        System.out.println(5%2.0);
//        System.out.println(5/6.0);
//
//        System.out.println((1+2+3)*4/5.0);
//
//        System.out.println(2*5+2*5+2*5+3*5*5*5);
        // '()' > '!' > '算数运算符'(* / % + - ) > '比较运算符'(> < >= <= == !=) > '逻辑运算符'(&& & || |) > '='
        System.out.println(!(5==6) || 6<4);

        int a = 0xF8;  // 二进制的 1111 1000
        int b = 0xF4;  // 二进制的 1111 0100
        int c = 0xF4;
        System.out.println(a);
        System.out.println(b);
        System.out.println(~c);
        System.out.println(a | b);
        System.out.println(5 & 1);
    }
}
