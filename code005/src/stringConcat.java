public class stringConcat {
    public static void main(String[] args) {
        // 字符串不是java中的基本数据类型, String 也不是保留字
        int a = 10;
        int b = 20;
        int c = a + b;
        System.out.println("a+b=" + c);
        boolean aBiggerThanB = a > b;
        System.out.println("a>b 是" + aBiggerThanB + "的");
        System.out.println("a+b="+ a + b);
        System.out.println("a+b=" + (a+b));
        System.out.println("a*b=" + a * b);
    }
}
