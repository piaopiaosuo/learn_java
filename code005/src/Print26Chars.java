public class Print26Chars {
    public static void main(String[] args) {
        char ch = 'A';
//        char ch = '\u5234';
        int num = ch;  // 自动类型转换,python中不会自动转换, 会报错
        System.out.println(num+"\t"+(char)(num++));  // 强制类型转换
        System.out.println(num+"\t"+(char)(num++));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
        System.out.println(num+"\t"+((char)(num++)));
    }
}
