public class variable {
    // 代码块, 语句, 表达式组成了java代码的三级跳
    // java中的关键字和标识符 是区分大小写
    // 类名必须与文件名一致,也是区分大小写的
    // 方法名和类型都是区分大小写的,不能写成Main和Int

    // 大括号都是代码块, 大括号括起来的零个或多个语句组成的就是代码块
    public static void main(String[] args){
        // int -2^31 ~ 2^31-1, -2.1 billion ~ 2.1 billion
        // 单纯创建一个变量
        int variable;
        // 给变量赋值, 不加分号的叫表达式expression, 加上分号叫语句statement,下面这个语句只有一个表达式
        variable = 2100000000;
        // 下面是两个表达式组合在一起的复杂表达式
        // int,x关键字, x标示符, =运算符, 5字面值, int数据类型, 变量的创建与赋值
        int a = 3;
        int b = 5;
        int c = 7;
        int x = 1;
        int y = a*x + b*x*x + c*x*x*x;
        long z = 9900000000l;
        System.out.println(z);
        // 下面是很多个表达式组成的语句
        System.out.println(y);
    }
}
