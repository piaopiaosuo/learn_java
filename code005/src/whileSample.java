public class whileSample {
    public static void main(String[] args) {
        long t1 = System.currentTimeMillis();
        int n = 50000;
        int dividend = 100;
        int divisor = 89;
        int found = 0;
        while(found<n){
            if (dividend%divisor == 0){
                found ++;
//                System.out.println(dividend+"可以正处"+divisor+", 商为："+dividend/divisor);
            }
            dividend += 1;
        }
        long t2 = System.currentTimeMillis();
        System.out.println((t2-t1)/1000.0);
//        System.out.println("done");/
    }
}
