public class data_type {
    public static void main(String[] args){
        System.out.println(1<2);
        System.out.println("abc");
        System.out.println('a');
        byte bytevar = 99;
        short shortvar = 9999;
        System.out.println(shortvar);
        long longvar = 99999999999L;
        System.out.println(longvar);
        float float_var = 123.456324f;
        double double_var = 123.45464576547546;
        System.out.println(float_var);
        System.out.println(double_var);
        char ch = 'a';
//        String ch = "abc";
        System.out.println(ch);

    }
}


// byte 1byte 8位
// short 2bytes 16位
// int 4bytes 32位  default
// long 8bytes 64位
// float 4bytes
// double 8bytes  default
// boolean 1byte
// char 2bytes
