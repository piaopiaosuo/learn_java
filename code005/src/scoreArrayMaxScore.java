//public class scoreArrayMaxScore {
//    public static void main(String[] args) {
//        int YuWen = 0;
//        int ShuXue = 1;
//        int WaiYu = 2;
//        int Wuli = 3;
//        int Huaxue = 4;
//        int Shengwu = 5;
//
//        int totleScoreCount = 6;
//        double[] socres = new double[totleScoreCount];
//        String[] scoreNames = new String[totleScoreCount];
//        scoreNames[YuWen] = "语文";
//        scoreNames[ShuXue] = "数学";
//        scoreNames[WaiYu] = "外语";
//        scoreNames[Wuli] = "物理";
//        scoreNames[Huaxue] = "化学";
//        scoreNames[Shengwu] = "生物";
//
//        for (int i = 0; i < totleScoreCount; i++) {
//            socres[i] = 80 + Math.random() * 20;
//            System.out.println(scoreNames[i] + "这门课的成绩是:" + socres[i]);
//        }
//
//        double maxScore = 0;
//        int maxScoreIndex = -1;
//
//        for (int i = 0; i < totleScoreCount; i++) {
//            if (socres[i] > maxScore) {
//                maxScore = socres[i];
//                maxScoreIndex = i;
//            }
//        }
//        System.out.println("最好成绩科目是:"+scoreNames[maxScoreIndex]+", 成绩是:"+maxScore);
//    }
//}

// 多维数组

import java.util.Scanner;

public class scoreArrayMaxScore {
    public static void main(String[] args) {
        int YuWen = 0;
        int ShuXue = 1;
        int WaiYu = 2;
        int Wuli = 3;
        int Huaxue = 4;
        int Shengwu = 5;

        int totleScoreCount = 6;

        String[] scoreNames = new String[totleScoreCount];
        scoreNames[YuWen] = "语文";
        scoreNames[ShuXue] = "数学";
        scoreNames[WaiYu] = "外语";
        scoreNames[Wuli] = "物理";
        scoreNames[Huaxue] = "化学";
        scoreNames[Shengwu] = "生物";

        Scanner in = new Scanner(System.in);
        System.out.println("请问要保存几年的成绩?");
        int yesToSave = in.nextInt();
        double[][] scores = new double[yesToSave][scoreNames.length];
        for (int j = 0; j < yesToSave; j++) {
            for (int i = 0; i < totleScoreCount; i++) {
                scores[j][i] = 80 + Math.random() * 20;
                System.out.println("第"+(j+1)+"年"+scoreNames[i] + "这门课的成绩是:" + scores[j][i]);
            }
        }

        System.out.println("请问要查看第几年的成绩?");
        int toSeeYear = in.nextInt();
        System.out.println("请问要查看哪一科的成绩?");
        int toSeeIndex = in.nextInt();
        System.out.println("你要查看的科目是:"+scoreNames[toSeeIndex]+",它的成绩是: "+scores[toSeeYear-1][toSeeIndex]);
//        double maxScore = 0;
//        int maxScoreIndex = -1;

//        for (int i = 0; i < totleScoreCount; i++) {
//            if (socres[i] > maxScore) {
//                maxScore = socres[i];
//                maxScoreIndex = i;
//            }
//        }
//        System.out.println("最好成绩科目是:" + scoreNames[maxScoreIndex] + ", 成绩是:" + maxScore);
    }
}