package com.phone;

import com.phone.parts.Mainboard;
import com.phone.parts.Phone;
import com.phone.parts.Screen;

public class myPhoneMarket {
    public static void main(String[] args) {
        Phone phone = new Phone();
        phone.hasFigurePrintUnlocker = true;
        phone.price = 1999;
        phone.screen = new Screen();
        phone.screen.producer = "京东方";
        phone.screen.size = 16;
        Mainboard mainboart = new Mainboard();
        mainboart.size = 32;
        phone.mainboard = mainboart;

    }
}
