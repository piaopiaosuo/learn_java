package com.geekbang;

import com.geekbang.person.Customer;
import com.geekbang.supermarket.LittleSuperMarket;
import com.geekbang.supermarket.Merchandise;

import java.util.Scanner;

public class RunLittleSupperMarketAppMain {
    public static void main(String[] args) {
        LittleSuperMarket littleSuperMarket = new LittleSuperMarket();
        littleSuperMarket.superMarketName = "有家小超市";
        littleSuperMarket.address = "浦东新区世纪大道666号";
        littleSuperMarket.parkingCount = 200;
        // 给超市200中商品
        littleSuperMarket.merchandises = new Merchandise[200];
        // 统计用的数组
        littleSuperMarket.merchandiseSold = new int[littleSuperMarket.merchandises.length];

        // 为了方便使用, 创建一个商品数组引用, 和littleSuperMarket.merchandises 指向同一个数组对象
        Merchandise[] all = littleSuperMarket.merchandises;

        // 遍历并给200中商品赋值
        for (int i = 0; i < all.length; i++) {
            Merchandise m = new Merchandise();
            m.name = "商品" + i;
            m.count = 200;
            m.id = "ID" + i;
            m.name = "商品" + i;
            m.purchasePrice = Math.random() * 200;
            m.soldPrice = (1 + Math.random()) * 200;
            all[i] = m;
        }
        System.out.println("超市开门了");

        boolean open = true;
        Scanner scanner = new Scanner(System.in);
        while (open) {
            System.out.println("本店叫做" + littleSuperMarket.superMarketName);
            System.out.println("本店地址" + littleSuperMarket.address);
            System.out.println("共有停车位" + littleSuperMarket.parkingCount);
            System.out.println("今天的营业额为" + littleSuperMarket.incomingSum);
            System.out.println("共有商品" + littleSuperMarket.merchandises.length);

            Customer customer = new Customer();
            customer.name = "顾客编号" + ((int) (Math.random() * 10000));
            customer.money = (1 + Math.random()) * 1000;
            System.out.println("顾客带了"+customer.money+"钱;");
            customer.isDrivingCar = Math.random() > 0.5;

            if (customer.isDrivingCar) {
                if (littleSuperMarket.parkingCount > 0) {
                    System.out.println("欢迎" + customer.name + "驾车而来,本店为您安排了车位,车位编号为" +
                            littleSuperMarket.parkingCount);
                    littleSuperMarket.parkingCount--;
                } else {
                    System.out.println("不好意思,本店车位已满,欢迎您下次光临");
                    continue;
                }
            } else {
                System.out.println("欢迎" + customer.name + "光临本店");
            }
            double totalCost = 0;
            while (true) {
                System.out.println("本店提供" + all.length + "中商品,欢迎选购");
                int index = scanner.nextInt();
                if (index < 0) {
                    break;
                }
                if (index > all.length){
                    System.out.println("本点没有这种商品, 请重新选购");
                    continue;
                }
                Merchandise m = all[index];
                System.out.println("您选购的商品名字" + m.name + ";单价为" + m.soldPrice + ".请问你要购买几个?");
                int numTobuy = scanner.nextInt();
                if (numTobuy <=0 || numTobuy *m.purchasePrice > customer.money){
                    System.out.println("您输入的数字有问题或者您的金额不够");
                    continue;
                }
                totalCost += numTobuy * m.soldPrice;
                customer.money -= totalCost;
                m.count -= numTobuy;
                littleSuperMarket.merchandiseSold[index] += numTobuy;

            }


            if (customer.isDrivingCar) {
                littleSuperMarket.parkingCount++;
            }
            System.out.println("顾客" + customer.name + "共消费" + totalCost);
            littleSuperMarket.incomingSum += totalCost;

            System.out.println("还想继续营业吗?");
            open = scanner.nextBoolean();
        }
        System.out.println("超市关门了");
        System.out.println("营业额" + littleSuperMarket.incomingSum + ".营业情况如下");
        for (int i = 0; i < littleSuperMarket.merchandiseSold.length; i++) {
            Merchandise m = all[i];
            int numSole = littleSuperMarket.merchandiseSold[i];
            if (numSole > 0) {
                double incomming = m.soldPrice * numSole;
                double netIncomming = (m.soldPrice - m.purchasePrice) * numSole;
                System.out.println(m.name + "售出了" + numSole + "个,销售额" + incomming + ".净利润为" + netIncomming);
            }
        }
    }
}
