public class reference {
    // java中的数据类型分为基本数据类型和引用数据类型

    /*  TODO 引用数据类型和基本数据类型的相同点
    都可以用来创建变量, 可以赋值和使用其值
    本身都是一个地址
     */

    /*  TODO 引用数据类型和基本数据类型的不同点
    基本类型变量的值,就是地址对应的值,引用数据类型的值还是一个地址,需要通过二级跳找到实例;
    引用数据类型是java的一种内部类型,是对所有自定义类型和数组引用的统称,并非特质某种类型.
     */

    /* TODO 类和对象的关系
    类是对象的模板, 对象是类的实例;  类定义了一类事物的属性,类就像一个表的表头,对象则是表中的一行数据
    一个java程序中的类型相对的类只能有一个,也就是类型不会重名
    一个类可以有多个对象
    一个对象只能根据一个类来创建
     */

    /* TODO 引用和类以及对象的关系
    引用必须是/只能是一个类的引用
    引用只能指向其所属的类型的类的对象
    相同类型的引用之间可以赋值
    只能通过指向一个对象的引用,来操作一个对象,比如访问某个成员变量
     */

    public static void main(String[] args) {
        // >> TODO m1 是一个Merchandise类型的引用,只能指向Merchandise类型的实例;
        // >> TODO 应用数据类型变量包含两部分信息: 类型和实例.也就是说:每一个引用数据类型的变量(简称引用),都是指向某个类(class/自定一类型)
        //    TODO 的一个实例/对象(instance/object). 不同类型的引用刚在java的世界里都是引用.
        // >> TODO 引用的类型信息在创建是就已经确定, 可以通过给引用赋值,让其指向不同的实例, 但是只能指向同一类型的实例,
        //    TODO 比如m1是Merchandise类型,只能指向Merchandise的实例.
        Merchandise m1;
        m1 = new Merchandise();
        Merchandise m2 = new Merchandise();
        Merchandise m3 = new Merchandise();
        Merchandise m4 = new Merchandise();
        System.out.println("m1="+m1);
        m1 = m4;
        int[] a = new int[5];
//        int m5 = 56;
//        double m6 = m5;  // 自动类型转换
//        System.out.println(m6);
    }
}
