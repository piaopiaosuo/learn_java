// >> TODO 一个类以public class 开头, public class 代表这个类是一个公共类,类名必须和文件名相同
// >> TODO public class 后面紧跟类名,然后是一对大括号的类体
// todo 可以变颜色, >>表示这是一个语法点而不是纯粹的程序注释
public class Merchandise {
    // >> TODO 类体中可以定义描述这个类属相的变量,我们称之为成员变量(member varialve)
    // >> TODO 每个成员变量的定义以;结束
    String name;
    String id;
    int count;
    double price;
}

// >> TODO 上面这整个类,其实就是创建了一个模板.描述了一种我们需要的数据类型.
