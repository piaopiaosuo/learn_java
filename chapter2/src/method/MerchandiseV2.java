package method;

public class MerchandiseV2 {
    public String name;
    public String id;
    // >> TODO　构造方法执行前，会执行局部变量付初始值的操作
    // >> TODO 我们说过，所有的代码都必须在方法里，　那么这种给成员变量付初始值的代码在哪个方法里？怎么看不到呢？
    // >> TODO 原来构造方法在内部变成了<init>方法．
    public int count = 999; // 999/0;
    public double soldPrice;
    public double purchasePrice;

    // >> TODO 静态变量使用static修饰符
    // >> TODO　静态变量如果不复制，ｊａｖａ也会给他赋予其类型的初始值
    // >> TODO 静态变量一般使用全大写字母加下划线分割，这是一个习惯用法
    // >> TODO　所有的点吗都可以使用静态变量，只要根据防范控制符的规范，这个静态变量对其可见即可
    //    TODO 比如ｐｕｂｌｉｃ的静态变量，所有的代码都可以使用它　静态变量相当于Ｐｙｔｈｏｎ中的静态字段,类的所有实例统一使用这个变量
    public static double DISCOUNT_FOR_VIP = 0.95;
    public static double VIP_COUNT = 0.95;
    public static double SVIP_COUNT = 0.95;

    // >> TODO　静态方法使用ｓｔａｔｉｃ修饰符
    // >> TODO 静态方法可以访问静态变量，包括自己类的静态变量和在访问修饰符允许的别的类的静态变量
    // >> TODO　除了没有ｔｈｉｓ，静态方法的定义和成员方法一样，也有方法名，返回值和参数
    // >> TODO 静态方法没有ｔｈｉｓ自引用，他不属于某个实例，调用的时候也无需引用，直接用类名调用，所以她不能直接访问成员变量
    // >> TODO　当然在静态方法里面，也可以自己创建对象，或者通过参数，获得对象的引用，进而调用方法和访问成员变量
    // >> TODO　静态方法只是没有ｔｈｉｓ自引用的方法而已
    public static double getDiscountOnDiscount(LittleSuperMarket littleSuperMarket) {
        double activityDiscount = littleSuperMarket.activaityDiscount;
        return DISCOUNT_FOR_VIP * activityDiscount;
    }

    public static double getDiscount() {
        return DISCOUNT_FOR_VIP;
    }

    public static double getDiscount(boolean isVIP) {
        // >> TODO 三元操作符
        // ｄｏｕｂｌｅ abc = true ? "" :0;
        double svipDiscount = (isVIP ? VIP_COUNT : 1);
        return getDiscount() * svipDiscount;
    }


    public static double getDiscount(int sVIPLevel) {
        double ret = getDiscount() * VIP_COUNT;
        for (int i = 0; i < sVIPLevel; i++) {
            ret *= SVIP_COUNT;
        }
        return ret;
    }

    // >> TODO 访问修饰符
    // >> TODO 返回值类型: 无需返回值则用void表示, void是java中的关键字
    // >> TODO 方法名: 人以合法的标识符都可以
    // >> TODO 参数列表: 后续讲解
    // >> TODO 方法体: 方法的代码

    // >> TODO 构造方法(constructor) 的方法名必须与类名一样, 而且构造方法没有返回值, 这样的方法才是构造方法
    // >> TODO 构造方法可以有参数,规则和语法与普通方法一样.使用时,参数传递给new语句后类名的括号后面;
    // >> TODO 如果没有显示的添加一个构造方法,java就会给每个类默认自带一个无参数的构造方法
    // >> TODO 如果我们自己添加类构造方法, java就不会在添加无参数的构造方法,这时候,,就不能new一个对象不传递参数了
    // >> todo 所以我们已在都在使用构造方法,这也是为什么创建对象的时候类名后面要有一个括号的原因
    // >> todo 构造方法无法被点操作符或者在普通方法里调用,只能通过new语句在创建对象的时候,间接调用
    // >> TODO 理解一下为什么都早方法不能有返回值,因为有返回值也没有意义, new 语句永远返回的事创建出来的对象的引用
    public MerchandiseV2(String name, String id, int count, double soldPrice, double purchasePrice) {
        this.name = name;
        this.id = id;
        this.count = count;
        this.soldPrice = soldPrice;
        this.purchasePrice = purchasePrice;
    }

    // >> TODO 在构造方法里才能调用重载的构造方法，语法为this(实参列表)
    // >> TODO 都早方法不能自己调用自己，这回是一个死循环
    // >> TODO 在调用重载的构造方法时，　不可以使用成员变量，因为从语义上讲，这个对象还没有被初始化完成，处于中间状态
    // >> TODO 在构造方法里才能调用重载的构造方法是，必须是方法的第一行，后面可依据须有代码
    public MerchandiseV2(String name, String id, int count, double soldPrice) {
        this(name, id, count, soldPrice, soldPrice * 0.8);
    }

    // >> TODO 因为我们添加了构造方法之后，　ｊａｖａ就不会在添加无参数的构造方法，如果需要的话，　我们可以自己添加这样的构造方法

    public MerchandiseV2() {
    }

    public void init(String name, String id, int count, double soldPrice, double purchasePrice) {

    }


//    public MerchandiseV2(String name) {
//        this.name = name;
//    }

    public void describe() {
        double me = soldPrice - purchasePrice;  // 对象的成员变量
        int m = (int) (me);
        System.out.println(m);
        System.out.println("商品名字叫做" + name + ",id是" + id + ",数量是" + count + ",毛利率是" + me);
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    // >> TODO 可以把有返回值的方法,当成一个成员变量, 当成一个类型为返回值类型的成员变量
    //    TODO 关注与返回值并忽略方法执行的部分, 使用返回值,就好像在使用一个成员变量
//    public double buy(int countToBuy) {
//        if (count < countToBuy) {
//            System.out.println("商品库存不足");
//            return -1;
//        }
//        System.out.println("商品单价为:" + soldPrice);
//        int fullPriceCount = countToBuy / 2 + countToBuy % 2;
//        int halfPriceCount = countToBuy - fullPriceCount;
//        System.out.println("全价商品为:" + fullPriceCount + ", 半价商品为" + halfPriceCount);
//        double allPrice = fullPriceCount * soldPrice + halfPriceCount * soldPrice / 2;
//        return allPrice;
//    }

    // >> todo 方法的签名和重载  方法的返回值不是方法签名的一部分: 因为返回值不能帮助确定调用哪个方法
    // >> TODO 重载的方法可以调用别的重载方法, 当然可以调用别的不重载的方法
    // >> TODO 实际上, 像这种补充一些缺省的参数值, 然后调用重载的方法,是重载的一个重要的使用场景.
    // >> TODO 在这里我们局的例子就是这样的,但是哦不是语法要求一定要这样, 重载的方法的方法体内代码可以随便
    //    TODO 可以不调用别的重载方法

//    public double buy() {
//        return buy(1);
//    }

    // >> TODO 依次使用byte,short..int, float, double 类型的参数调用不要buy方法, 哪方法会被调用呢?
    // >> TODO 无论是否重载参数类型可以不完全匹配的规则是"实参数可以自动类型转换为形参类型"
    // >> TODO 重载的特殊之处是, 参数满足自动类型转换的方法有好几个, 重载的规则是选择最"近"的去调用
    // >> TODO byte> short > int > long > float > double 向上兼容
    public double buy(int count) {
        System.out.println("int 正在被调用");
        return buy(count, false);
    }

//    public void buy(float count) {
//        System.out.println("double 正在被调用");
//        return buy(count, false);
//    }

    // >> TODO 最后都补充好方法,, 调用参数最全的一个方法
    public double buy(int count, boolean isVIP) {
        if (this.count < count) {
            return -1;
        }
        this.count -= count;
        double totalCost = count * soldPrice;
        if (isVIP) {
            return totalCost * DISCOUNT_FOR_VIP;
        } else {
            return totalCost;
        }
    }

    public void count(int count) {
        // >> TODO 方法里隐藏一个this自引用, 指向调用这个方法的对象
        // >> TODO 使用一个对象调用方法,也叫作在这个对象上调用方法.因为方法可以访问这个对象的值
        // >> TODO 访问一个成员变量的完整形态, 是"this.成员变量的名字"

        count = count * 2;
        System.out.println("this:" + this.count);
        System.out.println("this是:" + this);
        System.out.println("not this:" + count);
    }
}
