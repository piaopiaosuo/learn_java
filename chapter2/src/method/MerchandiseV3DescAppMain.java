package method;


public class MerchandiseV3DescAppMain {
    public static void main(String[] args) {
        MerchandiseV2 merchandise = new MerchandiseV2("商品", "23", 203, 324.12, 343.21);
        merchandise.name = "书桌";
        merchandise.id = "12";
        merchandise.soldPrice = 23.12;
        merchandise.purchasePrice = 11.2354;
        merchandise.describe();
    }
}
