package method;

import static method.MerchandiseV2.DISCOUNT_FOR_VIP;

import java.util.Scanner;

public class testMethod {
    public static void main(String[] args) {
        LittleSuperMarket littleSuperMarket = new LittleSuperMarket();
        littleSuperMarket.superMarketName = "有家小超市";
        littleSuperMarket.address = "浦东新区世纪大道666号";
        littleSuperMarket.parkingCount = 200;
        // 给超市200中商品
        littleSuperMarket.merchandises = new MerchandiseV2[200];
        // 统计用的数组
        littleSuperMarket.merchandiseSold = new int[littleSuperMarket.merchandises.length];

        // 为了方便使用, 创建一个商品数组引用, 和littleSuperMarket.merchandises 指向同一个数组对象
        MerchandiseV2[] all = littleSuperMarket.merchandises;

        // 遍历并给200中商品赋值
        for (int i = 0; i < all.length; i++) {
            MerchandiseV2 m = new MerchandiseV2("商品", "23", 203, 324.12, 343.21);
            m.init("商品", "23", 203, 324.12, 343.21);
//            m.setName("商品" + i);
//            m.setCount();
            m.name = "商品" + i;
            m.count = 200;
            m.id = "ID" + i;
            m.name = "商品" + i;
            m.purchasePrice = Math.random() * 200;
            m.soldPrice = (1 + Math.random()) * 200;
            m.DISCOUNT_FOR_VIP = 0.8;
            System.out.println(MerchandiseV2.DISCOUNT_FOR_VIP);
            System.out.println(DISCOUNT_FOR_VIP);
            all[i] = m;
            int aa = 12;
            m.buy(aa);
        }
        System.out.println("超市开门了");

        boolean open = true;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("本店提供" + all.length + "中商品,欢迎选购");
            int index = scanner.nextInt();
            if (index < 0) {
                break;
            }
            if (index > all.length) {
                System.out.println("本点没有这种商品, 请重新选购");
                continue;
            }
            MerchandiseV2 m = all[index];
            m.count(12);
            System.out.println(m.count);
            System.out.println("您选购的商品名字" + m.name + ";单价为" + m.soldPrice + ".请问你要购买几个?");
            int numTobuy = scanner.nextInt();
//            double allPrice = m.buy(numTobuy);
            m.count -= numTobuy;
            littleSuperMarket.merchandiseSold[index] += numTobuy;
        }
    }
}
